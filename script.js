$(function () {
    $("#codeButton").on("click", function (event) {
        event.preventDefault();
        var code = $("#sitzungscode").val();
        if (code != "") {
            var url = "https://frag.jetzt/participant/room/" + code;
            window.location.href = url;
        }
        else {
            $("#sitzungscode").addClass("incorrectInput");
        }
    });
});
