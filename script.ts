$(() => {

    $("#codeButton").on("click", (event) => {
        event.preventDefault();
        let code = $("#sitzungscode").val() as string;
        if(code != "") {
            let url = "https://frag.jetzt/participant/room/" + code;
            window.location.href = url;
        }else{
            $("#sitzungscode").addClass("incorrectInput")
        }
    })

})